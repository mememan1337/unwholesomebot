package main 

import (
	"github.com/bwmarrin/discordgo"

	"fmt"
	"flag"
	"os"
	"os/signal"
	"syscall"
	"strings"
	"reflect"
)

var oauth_token string
var cmd_prefix string = "!uwbot"

func init(){
	flag.StringVar(&oauth_token, "t", "", "Bot Token")
	flag.Parse()
}

func main(){
	
	fmt.Println(cmd_prefix)

	if oauth_token == "" {
		fmt.Println("No oauth token for bot providied. Please provide command line argument for the token using -t\n")
		return
	}

	dgo, err := discordgo.New("Bot " + oauth_token)

	if err != nil{
		fmt.Println("Error creating discord bot session: ", err)
		return
	}

	dgo.AddHandler(ready)
	dgo.AddHandler(messageCreate)


	err =dgo.Open()
	
	if err != nil {
		fmt.Println("Error opening discord bot session: ",err)
	}

	sc := make(chan os.Signal,1)
	signal.Notify(sc,syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill) 
	<-sc

	dgo.Close()
}

//Ready event handler
func ready(s *discordgo.Session, event *discordgo.Ready){
	s.UpdateStatus(0,"touching tips")
}

func messageCreate(s *discordgo.Session, msg *discordgo.MessageCreate){
	fmt.Println("Message created event triggered\n")

	//Ignore messages from the bot itself
	if msg.Author.ID ==  s.State.User.ID {
		return
	}

	//Check for Command Prefix
	if strings.HasPrefix(msg.Content, cmd_prefix){
		fmt.Println("Command Prefix recieved,tokenizing rest of command string\n")
		
		channel, err := s.State.Channel(msg.ChannelID)

		fmt.Println(reflect.TypeOf(channel))

		if err != nil{
			fmt.Println("Error getting channel ID\n")
			return 
		}

		guild, err :=  s.State.Guild(channel.GuildID)
		
		if err != nil {
			fmt.Println("Error getting guild for message\n")
			fmt.Println(guild)
			return
		}


		
		//tokens := strings.Split(msg.Content," ")
		tokens := tokenizeMessageContent(msg.Content)
		//Pop command prefix off tokens
		prefix, tokens := popTokens(tokens)
		//Pop action off tokens
		action, tokens := popTokens(tokens)
		fmt.Println(prefix)
		fmt.Println(action)
		performAction(action,s,msg.ChannelID)



	}

}

//Pops front of token slice return the pop and new slice
func popTokens(tokens [] string)(string,[] string){
	return tokens[0],tokens[1:]
}

//Seperates a space delimited discord message into tokens
func tokenizeMessageContent(msg string)([] string){

	if msg != ""{
		return strings.Split(msg," ")
	} else{
		return nil
	}


}

func performAction(action string, s *discordgo.Session,chan_ID string){
	

	if strings.Compare(action,"manifesto") == 0{
		sendMessage(s,chan_ID,"I am here to fuck and chew bubble gum. And I am all out of bubble gum")
	}

	if strings.Compare(action, "purpose") == 0{
		sendMessage(s,chan_ID,"National surveillance")
	}

	if strings.Compare(action,"mch") == 0{
		sendMessage(s,chan_ID,"I'm a cybernetic organism. Living tissue over a metal endoskeleton")
	}

}
func sendMessage(s *discordgo.Session,chan_ID string, msg string){
	s.ChannelMessageSend(chan_ID,msg)
}
